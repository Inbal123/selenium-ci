package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GooglePage {
    private final WebDriver driver;
    private final WebDriverWait wait;

    @FindBy(name = "q")
    private WebElement searchBox;

    @FindBy(name = "btnK")
    private WebElement searchButton;

    @FindBy(className = "LC20lb")
    private List<WebElement> searchResults;

    public GooglePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 10);
        PageFactory.initElements(this.driver, this);
    }

    public void goTo() {
        this.driver.get("https://www.google.com");
    }

    public void searchFor(String text) {
        this.searchBox.sendKeys(text);
        this.wait.until(ExpectedConditions.elementToBeClickable(this.searchButton));
        this.searchButton.click();
        this.wait.until(ExpectedConditions.presenceOfElementLocated(By.className("LC20lb")));
    }

    public List<WebElement> getResults() {
        return this.searchResults;
    }
}

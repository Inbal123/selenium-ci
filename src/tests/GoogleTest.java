package tests;

import java.net.MalformedURLException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.GooglePage;

public class GoogleTest extends BaseTest {
	private GooglePage googlePage;

	@Override
	@BeforeTest
	public void setUp() throws MalformedURLException {
		super.setUp();
		googlePage = new GooglePage(super.driver);
	}

	@Test(dataProvider = "search-keywords")
	public void googleTest(String searchKeyword) {
		googlePage.goTo();
		googlePage.searchFor(searchKeyword);
		Assert.assertTrue(googlePage.getResults().size() > 0);
	}

	@Override
	@AfterTest
	public void tearDown() throws InterruptedException {
		super.tearDown();
	}

	@DataProvider(name = "search-keywords")
	public static Object[][] credentials() {
		return new Object[][] {
			{ "test automation guru" },
			{ "selenium webdriver" },
			{ "dockerized selenium grid" },
			{ "test automation blog" },
			{ "jmeter docker" },
			{ "?????????????????????" }
		};
	}
}

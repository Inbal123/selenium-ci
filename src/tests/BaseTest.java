 package tests;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseTest {
	protected WebDriver driver;

	public void setUp() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "/jars/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		options.addArguments("--disable-gpu");
		options.addArguments("disable-infobars");
		options.addArguments("--disable-extensions");
		options.addArguments("window-size=1200x600");
		options.addArguments("--no-sandbox");

		//specify the hub URL           
		this.driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
	}

	public void tearDown() throws InterruptedException {
		this.driver.quit();
	}
}
